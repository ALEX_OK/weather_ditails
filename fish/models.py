from django.db import models


class Inform(models.Model):
    title = models.CharField(blank=True, max_length=150, verbose_name="Розповіді")
    content = models.TextField(blank=True)
    photo = models.ImageField(upload_to='photos/%Y/%m/%d', blank=True )
    photo2 = models.ImageField(upload_to='photos/%Y/%m/%d', blank=True )
    time_create = models.DateTimeField(auto_now_add=True, verbose_name="створено")
    time_update = models.DateTimeField(auto_now=True, verbose_name="оновлено")
    is_published = models.BooleanField(default=True, verbose_name="опубліковано")
    category1 = models.ForeignKey("Category",  on_delete=models.CASCADE, null=True, blank=True)
    photo1 = models.ForeignKey("Foto",  on_delete=models.SET_NULL, null=True, blank=True)
    # coment = models.ForeignKey('Coments',  on_delete=models.SET_NULL, null=True, blank=True)



    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Розповідь'
        verbose_name_plural = 'Розповіді'
        ordering = ['-time_create','-time_update']



class Category(models.Model):
    title = models.CharField( max_length=150, verbose_name="Ім'я Категорія", db_index=True )    # inform = models.ForeignKey(Inform,  on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Категорія'
        verbose_name_plural = 'Категорії'
        ordering = ['id']


class Foto(models.Model):    
    title = models.CharField(blank=True, max_length=150, verbose_name="ФОТО", db_index=True )
    photos = models.ImageField(upload_to='photos/%Y/%m/%d', blank=True )
    # inform1 = models.ManyToManyField(Inform, through=Category)


    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Фото'
        verbose_name_plural = 'Фотографії'
        ordering = ['-id']


class Coments(models.Model):
    # title = models.CharField(blank=True, max_length=150, verbose_name="Коментар")
    # user_name =models.CharField(blank=True, max_length=150, verbose_name="Ім'я")
    comment = models.TextField(blank=True)
    time_create = models.DateTimeField(auto_now_add=True, verbose_name="створено")
    updated = models.DateTimeField(auto_now=True, blank=True)
    post = models.ForeignKey(Inform, on_delete=models.CASCADE, null=True, related_name='comments', blank=True)
    active = models.BooleanField(default=False, blank=True)

    # def __str__(self):
    #     return self.comment

    class Meta:
        verbose_name = 'Коментар'
        verbose_name_plural = 'Коментарі'
        ordering = ['-id']
