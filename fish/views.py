import sqlite3
import datetime
from django.http import HttpResponseNotFound,HttpResponse, Http404
from django.shortcuts import render, redirect
from weather_ditails.weather_inform import weather
from .models import *


def index(request):
	return render(request, 'index.html', {'title':'Погода'})


def resault(request):
    name_city = request.GET["city"]
    temperature, wind_speed, wind_der, cloudy, humidity, pressure, point, coment = weather(name_city)
    context = {
        'title':f'Погода в {name_city}',
        'name_city':name_city,
        'temperature': temperature,
        'wind_speed':wind_speed,
        'wind_der':wind_der,
        'cloudy':cloudy,
        'humidity':humidity,
        'pressure':pressure,
        'point':point,
        'coment':coment,
    }
    return render(request, 'resault.html', context)


def inform(request):
    return render(request, 'inform1.0.html',{'title':'Корисна інформація'})

# def archive(request, year):
#     if int(year) > 2020:
#         return redirect('/', permanent=True)
#         # raise Http404()
#     return HttpResponse(f"<h1>Архив по годам</h1><p>{year}</p>")

# def categories(request, catid):
#
#     if request.POST:
#         print(request.POST)
#     return HttpResponse(f'<h1>Статьи по категориям</h1><p>{catid}</p>' )


def pageNotFound(request, exception):
    return HttpResponseNotFound('<h1>Страница не найдена</h1>')


def about(request):
    return render(request, 'about.html', {'title':'про нас'})


def story(request):
    conn = sqlite3.connect('db.sqlite3')
    cursor = conn.cursor()
    if request.GET:
        coment = request.GET["comment"]
        date = datetime.datetime.now()
        cursor.execute("INSERT INTO fish_coments (comment, time_create) VALUES (?,?)", (coment,date))
    # user = cursor.execute(SELECT * FROM auth_user;)
    # coment = cursor.execute("SELECT comment FROM fish_coments;")
    conn.commit()
    conn.close()
    posts = Inform.objects.all()
    fotos = Foto.objects.all()
    coment_data  = Coments.objects.all()

    return render(request, 'story.html',{'coment_data':coment_data,'posts':posts,'fotos':fotos, 'title':'історія з рибалки'} )




