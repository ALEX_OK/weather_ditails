from django.urls import path, re_path

from .views import *

urlpatterns = [
	path('', index, name='home'),
    path('resault/',resault, name='resault'),
    path('inform/', inform, name='inform'),
    path('about/', about, name='about'),
    path('story/',story, name='story'),
    # path('cats/<int:catid>/',categories),
    # re_path(r'^archive/(?P<year>[0-9]{4})/', archive)
]
