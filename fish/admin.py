from django.contrib import admin


from .models import Inform, Category, Foto, Coments



class InformAdmin(admin.ModelAdmin):
	list_display = ('id','title','photo','photo1','photo2','category1','time_create', 'time_update', 'is_published')
	list_display_links = ('id','title')
	search_fields = ('title','content', 'foto')


class CategoryAdmin(admin.ModelAdmin):
	list_display = ('id','title')
	list_display_links = ('id','title')
	search_fields = ('title',)


class FotoAdmin(admin.ModelAdmin):
	list_display = ('id','title', 'photos' )
	list_display_links = ('id','title')
	search_fields = ('title',)


class ComentsAdmin(admin.ModelAdmin):
	list_display = ('id','post', 'comment', 'time_create','active')
	list_display_links = ('id',)
	search_fields = ('id',)
	ordering = ('time_create',)


admin.site.register(Inform,InformAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Foto, FotoAdmin)
admin.site.register(Coments, ComentsAdmin)


