from datetime import datetime, timedelta, timezone
import requests
import json


def weather(city):
    get_request = requests.get(url=f'https://api.openweathermap.org/data/2.5/forecast?units=metric&q={city}&appid=ab84206fcabe62472715d45f15c9409e&lang=ua')
    data = get_request.json()
    data2 = data['list']
    data3 = data2[0]
    first_index = data3
    dict_main = first_index['main']
    dict_weather = first_index['weather']
    list_weather = dict_weather[0]
    dict_wind = first_index['wind']
    dict_clouds = first_index['clouds']
    description = list_weather['description']
    cloudy = dict_clouds['all']
    date_2n = first_index['dt_txt']
    wind_speed = round(dict_wind['speed'],1)
    wind_der = dict_wind['deg']
    temperature = round(dict_main['temp'])
    pressure = round(dict_main['pressure'] * 0.75)
    humidity = dict_main['humidity']


    def wind_derect(wind_der):
        if 23 <= wind_der <= 67:
            pro = "Північно-східний"
            return pro
        elif 67 <= wind_der <= 113:
            pro = "Східний"
            return pro
        elif 113 <= wind_der <= 158:
            pro = "Південно-східний"
            return pro
        elif 158 <= wind_der <= 203:
            pro = "Південний"
            return pro
        elif 203 <= wind_der <= 248:
            pro = "Південно-західний"
            return pro
        elif 248 <= wind_der <= 293:
            pro = 'Західний'
            return pro
        elif 293 <= wind_der <= 338:
            pro = 'Північно-західний'
            return pro
        elif 338 <= wind_der < 360 or 0 <= wind_der < 23:
            pro = 'Північний'
            return pro


    def score(pressure,temp,pro,speed,cloud):
        score_pressure = 0
        if 762 < pressure < 770:
            score_pressure = 5
        elif 761 < pressure < 765:
            score_pressure = 10
        elif 755 < pressure < 760:
            score_pressure = 20

        score_temperature = 0
        if 23 < temp <= 35:
            score_temperature = 5
        elif 30 < temp < 32 or 21 < temp < 23:
            score_temperature = 10
        elif 24 < temp < 29:
            score_temperature = 20

        score_wind_der = 0
        if pro == 'Північний' or pro == 'Східний':
            score_wind_der = 5
        elif pro == 'Південно-східний' or pro == 'Південний' or pro == 'Південно-західний' or pro == 'Західний':
            score_wind_der = 20
        elif pro == 'Північно-східний' or pro == 'Північно-західний' or pro == 'Західний':
            score_wind_der = 10

        score_wind_speed = 0
        if 0 <= speed < 5:
            score_wind_speed = 20
        elif 5 < speed <= 8:
            score_wind_speed = 10
        elif speed < 10:
            score_wind_speed = 5

        score_clouds = 0
        if 0 <= cloud < 33:
            score_clouds = 20
        elif 33 < cloud < 67:
            score_clouds = 10
        elif 67 < cloud < 100:
            score_clouds = 5
        final_score = int(score_clouds) + int(score_pressure) + int(score_temperature) + int(score_wind_der) + int(
            score_wind_speed)
        return final_score


    def final_text(final_score):
        text = ''
        if final_score <= 35:
            text = ' - Умови для рибалки не сприятливі'
        elif 40 <= final_score <= 65:
            text = ' - Середньо-сприятливі умови для рибалки'
        elif 70 <= final_score <= 100:
            text = ' - Сприятливі умови для рибалки'

        return text

    wind_der = wind_derect(wind_der)
    points = score(pressure,temperature,wind_der,wind_speed,cloudy)
    coment = final_text(points)
    return temperature, wind_speed, wind_der, cloudy, humidity, pressure, points,coment